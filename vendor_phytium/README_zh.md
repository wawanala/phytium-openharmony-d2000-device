# vendor_phytium

## 介绍

该仓库对应phytium硬件产品d2000。

## 目录

```
vendor/phytium
└── ...
```

## 相关仓

* [device/board/phytium](https://gitee.com/openharmony/device_board_phytium)
* [device/soc/phytium](https://gitee.com/openharmony/device_soc_phytium)
