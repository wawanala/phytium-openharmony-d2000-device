# phytium hardware组件

#### 简介

媒体及wifi南向接口实现、框架及对接层库目录。

#### 约束

目前支持d2000

#### 对应仓库

1.  device_soc_phytium/hardware/display
2.  device_soc_phytium/hardware/gpu
3.  device_soc_phytium/hardware/isp
4.  device_soc_phytium/hardware/mpp
5.  device_soc_phytium/hardware/rga
6.  device_soc_phytium/hardware/wifi
