# rockchip hardware

## Introduction

Media and wifi southbound interface implementation, framework and docking layer library directory。

## Constraints

Currently, d2000 are supported.

## Repositories Involved
1.  device_soc_phytium/hardware/display
2.  device_soc_phytium/hardware/gpu
3.  device_soc_phytium/hardware/isp
4.  device_soc_phytium/hardware/mpp
5.  device_soc_phytium/hardware/rga
6.  device_soc_phytium/hardware/wifi
