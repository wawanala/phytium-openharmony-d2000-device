# 飞腾d2000 EVB

**一、飞腾d2000 EVB简介**

飞腾d2000 EVB上搭载了飞腾兼容ARMV8架构指令集8核CPU与自研GPU X100。

**二、搭建开发环境**

**三、安装依赖工具**

安装命令如下：

sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex
bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib
libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache
libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8
python3-pip ruby

**说明：** 
以上安装命令适用于Ubuntu18.04，其他版本请根据安装包名称采用对应的安装命令。

**2、获取标准系统源码**

**前提条件**

1）注册码云gitee账号。

2）注册码云SSH公钥，请参考[码云帮助中心](https://gitee.com/help/articles/4191)。

3）安装[git客户端](http://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%AE%89%E8%A3%85-Git)和[git-lfs](https://gitee.com/vcs-all-in-one/git-lfs?_from=gitee_search#downloading)并配置用户信息。

git config --global user.name "yourname"

git config --global user.email "your-email-address"

git config --global credential.helper store

4）安装码云repo工具，可以执行如下命令。

curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 \>
/usr/local/bin/repo \#如果没有权限，可下载至其他目录，并将其配置到环境变量中

chmod a+x /usr/local/bin/repo

pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests

**获取源码操作步骤**

1） 通过repo + ssh 下载（需注册公钥，请参考码云帮助中心）。

repo init -u git@gitee.com:openharmony/manifest.git -b master --no-repo-verify

repo sync -c

repo forall -c 'git lfs pull'

2） 通过repo + https 下载。

repo init -u https://gitee.com/openharmony/manifest.git -b master
\--no-repo-verify

repo sync -c

repo forall -c 'git lfs pull'

**执行prebuilts**

在源码根目录下执行脚本，安装编译器及二进制工具。

bash build/prebuilts_download.sh

下载的prebuilts二进制默认存放在与OpenHarmony同目录下的OpenHarmony_2.0_canary_prebuilts下。

**四、编译**

**1、编译**

在Linux环境进行如下操作:

1） 进入源码根目录，执行如下命令进行版本编译。

./build.sh --product-name d2000 --ccache --target-cpu arm64  

2） 检查编译结果。编译完成后，log中显示如下：

post_process

=====build d2000 successful.

xxxx-xx-xx xx:xx:xx

编译所生成的文件都归档在out/d2000/目录下，结果镜像输出在
out/d2000/packages/phone/images/ 目录下。

3） 定制ramdiskfs  
   $mkdir ~/ramdisk  
   $cp $PROJ_ROOT/device/board/phytium/d2000/loader/ramdisk/initramfs_data.cpio ~/ramdisk  
   $cd ~/ramdisk  
   $sudo cpio -idmv < initramfs_data.cpio  
   $rm initramfs_data.cpio  
   $cp $PROJ_ROOT/out/d2000/package/phone/ramdisk/\* . -rf  
   $cp $PROJ_ROOT/device/soc/phytium/hardware/gpu/mesa3d/lib/ . -r  
   $sudo find . | cpio -o -Hnewc > ../initramfs.img  

**五、镜像烧录**

**1、硬盘分区**  
1） 一共涉及到4个分区，依次为boot，system，vendor,userdata，根据实际情况设定，比如可以设定为500M，3G，500M，10G。  
2） 启动方案分为Uboot和UEFI两种方式，根据板子搭载的启动固件选择。UEFI启动方式，要求第一个分区必须为EFI格式，具体方法参考飞腾嵌入式linux用户手册，UEFI启动方案。  
3） system,vendor,user分区，都为EXT4格式，使用dd命令烧写,sdX中的X烧写之前先用df命令确认一下是多少，可能是b，c...等，这里一定要注意不要烧错。  
     $sudo dd if=system.img of=/dev/sdX2 bs=1M  
     $sudo dd if=vendor.img of=/dev/sdX3 bs=1M  
     $sudo dd if=userdata.img of=/dev/sdX4 bs=1M  
4） boot分区烧写的方式，根据不同方案参考 2、Uboot开机方案 或 3、 UEFI开机方案  

**2、Uboot开机方案**  
1） copy 内核镜像,设备树，ramdisk镜像到boot分区  
2） 设置开机环境变量  
在开机阶段，按回车，设置uboot环境变量  
设置启动参数  
setenv bootargs console=ttyAMA1,115200 earlycon=p1011,0x28001000 root=/dev/ram0 elevator=deadline rootwait rw loglevel=6 hardware=d2000 rootfstype=ext4 initrd=0x93000000,90M  

设置启动命令  
setenv bootcmd "ext4load scsi 0:1 0x90100000 d2000-devboard-dsk.dtb;ext4load scsi 0:1 0x90200000 Image;ext4load scsi 0:1 0x93000000 initramfs.img;booti 0x90200000 - 0x90100000"  

保存环境变量  
saveenv  
3） 将第一个分区格式成EXT4格式，并其名为boot  
    $sudo mkfs.ext4 -L boot /dev/sdX1  
4） copy kernel镜像  
    $sudo cp $PROJ_ROOT/device/board/phytium/d2000/loader/Image  /media/{yourname}/boot  
5） copy 设备树文件  
    $sudo cp $PROJ_ROOT/device/board/phytium/d2000/loader/d2000-devboard-dsk.dtb /media/{yourname}/boot  
6） copy 前面定制好的ramfs镜像  
    $sudo cp ~/initramfs.img /media/{yourname}/boot   
7） 接好硬盘，检查硬件连接，开机上电  

**3、UEFI开机方案**  
1） mount EFI分区到/mnt  
    $sudo mount /dev/sdX1 /mnt  
2） copy uefi固件  
    $sudo cp $PROJ_ROOT/device/board/phytium/d2000/loader/EFI /mnt -r  
3） copy kernel镜像  
    $sudo cp $PROJ_ROOT/device/board/phytium/d2000/loader/Image /mnt  
4） copy 设备树文件  
    $sudo cp $PROJ_ROOT/device/board/phytium/d2000/loader/d2000-devboard-dsk.dtb /mnt  
5） copy 前面定制好的ramfs镜像  
    $sudo cp ~/initramfs.img /mnt  
6） 如果有修改启动参数需求，可通过修改实现，默认不需要修改这个文件  
    $sudo vi /mnt/EFI/BOOT/grub.cfg  
7） umount   
    $sudo umount /mnt  
8） 接好硬盘，检查硬件连接，开机上电  
