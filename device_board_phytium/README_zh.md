# device_board_phytium

## 简介

#### phytium简介

#### 开发板简介

## 目录

```
device/board/phytium
├── d2000
└── ...
```

## 使用说明

d2000参考：
- [d2000](https://gitee.com/openharmony/device_board_phytium/d2000README_zh.md)

## 相关仓

* [vendor/phytium](https://gitee.com/openharmony/vendor_phytium)
* [device\soc\phytium](https://gitee.com/openharmony/device_soc_phytium)
* [device\soc\phytium](https://gitee.com/openharmony/device_soc_phytium)
