# Phytium-OpenHarmony-D2000-device

#### 介绍
该项目介绍，如何在天津飞腾信息科技有限公司的 Phytium D2000+X100 DEV平台上运行OpenHarmony标准系统。
X100支持视频解码硬件加速，图形显示硬件加速。

# 搭建开发环境
## 1.1 硬件环境
准备一台装有ubuntu20.04系统X86主机，内存最低配置要求16G。 
## 1.2 下载repo脚本文件
1. 注册码云gitee账号。

2. 注册码云SSH公钥，请参考[码云帮助中心](https://gitee.com/help/articles/4191)。

3. 安装[git](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.2-beta1.md#https://gitee.com/link?target=https%3A%2F%2Fgit-scm.com%2Fbook%2Fzh%2Fv2%2F%25E8%25B5%25B7%25E6%25AD%25A5-%25E5%25AE%2589%25E8%25A3%2585-Git)客户端和[git-lfs](https://gitee.com/vcs-all-in-one/git-lfs?_from=gitee_search#downloading)并配置用户信息。
```
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global credential.helper store
```
4. 安装码云repo工具，可以执行如下命令。
```
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo  #如果没有权限，可下载至其他目录，并将其配置到环境变量中chmod a+x /usr/local/bin/repo
pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```
## 1.3 获取OpenHarmony标准系统源码
推荐版本[OpenHamony 3.2 Beta2](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.2-beta2.md)。

通过repo + ssh 下载（需注册公钥，请参考[码云帮助中心](https://gitee.com/help/articles/4191)）。

```
export WORK_SPACE=/home/xxx/OpenHarmony #替换成自己定义的workspace路径
export PROJ_ROOT=$WORK_SPACE/3.2-Beta2
mkdir $WORK_SPACE
mkdir $PROJ_ROOT
cd &PROJ_ROOT
repo init -u git@gitee.com:openharmony/manifest.git -b OpenHarmony-3.2-Beta2 --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```
## 1.4 获取编译工具链
参考[OpenHamony快速入门文档](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta/zh-cn/device-dev/quick-start/quickstart-standard-running-rk3568-build.md)，使用安装包方式获取编译工具链。
```
sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8 python3-pip ruby
```
## 1.5 执行prebuilts
在源码根目录下执行脚本，安装编译器及二进制工具。
```
cd &PROJ_ROOT
bash build/prebuilts_download.sh
```

# 代码下载与整合
## 2.1 下载phytium device源码
创建存放phytiym device源码的目录，进到该目录，下载phytium dvice源码：
```
export PHY_DEV=$WORK_SPACE/phytium_device
mkdir $PHY_DEV
cd $PHY_DEV
git clone git@gitee.com:phytium_embedded/phytium-openharmony-d2000-device.git
```
## 2.2 整合phytiym device源码
将获取到的device源码分别放入OpenHarmony的device对应的目录
```
cp $PHY_DEV/device_board_phytium $PROJ_ROOT/device/board/phytium -r
cp $PHY_DEV/device_soc_phytium $PROJ_ROOT/device/soc/phytium -r
cp $PHY_DEV/vendor_phytium $PROJ_ROOT/vendor/phytium -r
```

# 代码编译
## 3.1 OpenHarmony编译  
```
cd $PROJ_ROOT  
./build.sh --product-name d2000 --ccache --target-cpu arm64  
```
 编译成功提示:
```
post_process
=====build d2000 successful.
```
编译生成的文件
```
$PROJ_ROOT/out/d2000/packages/phone/images/system.img 
$PROJ_ROOT/out/d2000/packages/phone/images/vendor.img
$PROJ_ROOT/out/d2000/packages/phone/images/userdata.img
```
## 3.2 Kernel编译
phytium device提供了kernel预编译好的文件：
```
$PHY_DEV/device_board_phytium/d2000/loader  
```
也可以联系phytium公司获取kernel源码，并参考kernel源码中的README，编译出相关文件。

## 3.3 Ramdisk编译
```
export RAMDISK=$WORK_SPACE/ramdisk
mkdir $RAMDISK
cd $RAMDISK
cp $PROJ_ROOT/device/board/phytium/d2000/loader/ramdisk/initramfs_data.cpio $RAMDISK  
sudo cpio -idmv < initramfs_data.cpio  
rm initramfs_data.cpio  
cp $PROJ_ROOT/out/d2000/packages/phone/ramdisk/* $RAMDISK -rf  
cp $PROJ_ROOT/device/soc/phytium/d2000/hardware/gpu/mesa3d/lib/ $RAMDISK -r  
sudo find . | cpio -o -Hnewc > ../initramfs.img  
```

# 镜像烧写
## 4.1 硬盘分区
准备一块SATA硬盘，删除原有分区后，在linux下，使用fdsik命令分区，创建4个分区，依次为boot，system，vendor，userdata，根据实际情况设定，比如可以设定为500M，3G，500M，10G。  
p1 500MB for BOOT/EFI  
p2 3GB for system  
p3 500MB for vendor  
p4 10G for data  
>fdisk命令详细使用方法可自行百度谷歌，或者参考[飞腾嵌入式 LINUX 用户
手册](https://gitee.com/phytium_embedded/phytium-sdk/blob/master/%E9%A3%9E%E8%85%BE%E5%B5%8C%E5%85%A5%E5%BC%8FLinux%E7%94%A8%E6%88%B7%E6%89%8B%E5%86%8C.pdf)中的磁盘分区部分内容。
## 4.2 system/vendor/userdata分区
将这三个分区并格式化为ext4，sdX中的X烧写之前先用df命令确认一下是多少，可能是b，c...等，这里一定要注意不要烧错。
```
sudo mkfs.ext4 sdx2
sudo mkfs.ext4 sdx3
sudo mkfs.ext4 sdx4
```
使用dd命令将[3.1 OpenHarmony编译](#31-openharmony编译)章节编译生成的镜像文件烧写到对应分区中。
```
sudo dd if=system.img of=/dev/sdX2 bs=1M
sudo dd if=vendor.img of=/dev/sdX3 bs=1M
sudo dd if=userdata.img of=/dev/sdX4 bs=1M   
```
## 4.3. boot分区   
boot分区的烧录，启动方式不一样，烧录方法不一样，区分为Uboot启动和UEFI启动两种。
### 4.3.1 Uboot启动
1. 将第一个分区格式化为ext4
```
sudo mkfs.ext4 sdx1
```
2. 挂载boot分区
```
mkdir ~/disk
sudo mount /dev/sdx1 ~/disk
```
3. 将[3.2 Kernel编译](#32-kernel编译)章节中生成的Kernel镜像，设备树文件，以及[3.3 Ramdisk编译](#33-ramdisk编译)章节中生成的ramdisk拷贝到boot分区。  
以直接使用预编译好的文件为例：
```
sudo cp $PHY_DEV/device_board_phytium/d2000/loader/Image ~/disk/
sudo cp $PHY_DEV/device_board_phytium/d2000/loader/d2000-devboard-dsk.dtb ~/disk/
sudo cp $WORK_SPACE/initramfs.img ~/disk/
sync
```
>如果使用kernel源码编译的文件，参考kernel源码中提供的README。
4. 卸载boot分区
```
sudo umount ~/disk
```
### 4.3.2 UEFI启动
1) 将第一个分区格式化为EFI
```
sudo mkfs.vfat sdx1
```
2) 挂载EFI分区
```
mkdir ~/disk
sudo mount /dev/sdx1 ~/disk
```
3) 将[3.2 Kernel编译](#32-kernel编译)章节中生成的Kernel镜像，设备树文件，EFI文件，以及[3.3 Ramdisk编译](#33-ramdisk编译)章节中生成的ramdisk拷贝到EFI分区。
以直接使用预编译好的文件为例：
```
sudo cp $PHY_DEV/device_board_phytium/d2000/loader/Image ~/disk/
sudo cp $PHY_DEV/device_board_phytium/d2000/loader/d2000-devboard-dsk.dtb ~/disk/
sudo cp $PHY_DEV/device_board_phytium/d2000/loader/EFI/ ~/disk/ -r
sudo cp $WORK_SPACE/initramfs.img ~/disk/
sync
```
>如果使用kernel源码编译的文件，参考kernel源码中提供的README。

如果需要修改启动参数，修改~/disk/EFI/BOOT/grub.cfg文件，一般使用默认启动参数的即可。

4) 卸载boot分区
```
sudo umount ~/disk
```

# 设备启动

将烧写好的SATA硬盘, 连接串口线，连接到d2000 EVB板上。上位机的串口调试工具波特率设置为115200，上电开机。  
## 5.1 Uboot启动  
在开机阶段，按回车，设置uboot环境变量
```
setenv bootargs console=ttyAMA1,115200 earlycon=p1011,0x28001000 root=/dev/ram0 elevator=deadline rootwait rw loglevel=6 hardware=d2000 rootfstype=ext4 initrd=0x93000000,90M
ext4load scsi 0:1 0x90100000 d2000-devboard-dsk.dtb
ext4load scsi 0:1 0x90200000 Image
ext4load scsi 0:1 0x93000000 initramfs.img
booti 0x90200000 - 0x90100000
```
## 5.2 UEFI启动
启动参数已经在烧写EFI分区时已写好，上电后，会直接进入系统。

# 遗留问题
   

# 开源许可协议  
Apache 2.0  

# 维护者邮箱 
飞腾信息技术有限公司  
zhangjianwei@phytium.com.cn  
tangkaiwen@phytium.com.cn  
gengjuqiang0937@phytium.com.cn  
xiayan1086@phytium.com.cn  
libowen1180@phytium.com.cn  
